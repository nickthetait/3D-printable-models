# 3D Printable models

## Instructions
* Clone/download the files you want
* Open in OpenSCAD
* Customize to your needs
* Export your hand crafted (by a :computer:) model
* Import to your 3D printing software
* Print away ❗
* For bonus points share a picture of your creation on social media :heart_eyes:

## Items of note
* Some of these designs have been reverse engineered from an original using calipers and OpenSCAD
