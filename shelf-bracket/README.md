![Image](media/Render.png)

# Notes
Nick Tait's massively over-engineered shelf peg support generator toolkit
or NTMOESPSGT for short.

This endeavour was concieved for a workshop on reverse engineering (to be) taught at DEF CON 26.

# Attribution
Builds upon the work of ![Zach Archer (aka zkarcher)](https://www.thingiverse.com/thing:2813759) and ![Jeremy Fischer (aka jnfischer)](https://www.thingiverse.com/thing:108222).

# Recommended settings for 3D Printing:
- Material: Pretty much anything rigid should work