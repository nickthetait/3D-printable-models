use <design.scad>;

offset_dist = 50;

color("blue")
translate([0, 0, 0])
main(cutout_type = "curved");

color("red")
translate([offset_dist, 0, 0])
main(catch = true, brace_type = "full", cutout_type = "curved");

color("green")
translate([0, offset_dist, 0])
main(brace_type = "double");

color("white")
translate([offset_dist, offset_dist, 0])
main(brace_type = "full");