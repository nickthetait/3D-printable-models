use <design.scad>;

length = 15;
width = 12;
thickness = 2;
hole_diameter = 6;
hole_depth = 5.5;
peg_sides = 8;
catch = true;
shelf_height = 5;
brace_type = "full";
cutout_type = "line";

module colorized()
{
    rotate([90,0,0])
    {
        color("Chocolate")
        support(length, width, thickness);

        color("RoyalBlue")
        brace_type(length, width, thickness, brace_type, cutout_type);

        color("lime")
        peg(width, thickness, hole_diameter, hole_depth, peg_sides);

        color("darkviolet")
        catch(width, thickness, shelf_height);
    }
}

module separated()
{
    move = 7;
    rotate([90,0,0])
    {
        support(length, width, thickness);

        translate([move,0,move])
        brace_type(length, width, thickness, brace_type, cutout_type);

        translate([-move,0,0])
        peg(width, thickness, hole_diameter, hole_depth, peg_sides);

        translate([0,0,-move])
        catch(width, thickness, shelf_height);
    }
}

separated();